<?php

/**
 * @file
 * Template file for gallery.
 */

?>

<script type="text/javascript">

var configOptions;
var modulePath;

function setConfigOptions(groupID, mapID, thePath) {
  modulePath = thePath;
  
  configOptions = {
   "modulePath": thePath,
   "group":groupID,
   "webmap": mapID,
   "appid":"",
   "theme":"blueTheme",
   "siteTitle":"",
   "siteBannerImage":"",
   "mapTitle":"",
   "mapSnippet":"",
   "mapItemDescription":"",
   "homeHeading":"",
   "homeSnippet":"",
   "homeSideHeading":"Description",
   "homeSideContent":"",
   "footerHeading":"",
   "footerDescription":"",
   "footerLogo":"",
   "footerLogoUrl":"",
   "addThisProfileId":"xa-4f3bf72958320e9e",
   "defaultLayout":"list",
   "sortField":"modified",
   "sortOrder":"desc",
   "searchType":"Web Map",
   "mapViewer":"drupal",
   "galleryItemsPerPage":9,
   "showFullScreenButton": false,
   "showProfileUrl":true,
   "showAboutPage":false,
   "showSocialButtons":true,
   "showFooter":false,
   "showBasemapGallery":false,
   "showGroupSearch":true,
   "showMapSearch":false,
   "showLayerToggle":true,
   "showLayoutSwitch":true,
   "showOverviewMap":true,
   "showMoreInfo":true,
   "showPagination":true,
   "showExplorerButton":false,
   "showArcGISOnlineButton":false,
   "showMobileButtons":false,
   "openGalleryItemsNewWindow":false,
   "bingMapsKey":"",
   "proxyUrl":"",
   "locatorserviceurl":"http://tasks.arcgis.com/ArcGIS/rest/services/WorldLocator/GeocodeServer/",
   "geometryserviceurl":"http://tasks.arcgisonline.com/ArcGIS/rest/services/Geometry/GeometryServer",
   "sharingurl":"http://www.arcgis.com/sharing/rest/content/items",
   "portalUrl":"http://www.arcgis.com/",
   "mobilePortalUrl":"arcgis://www.arcgis.com/",
   "iosAppUrl":"itms://itunes.apple.com/us/app/arcgis/id379687930?mt=8",
   "androidAppUrl":"https://market.android.com/details?id=com.esri.android.client",
   "pointGraphic":"images/ui/bluepoint-21x25.png"
  }
}
</script>

<!-- STYLESHEETS -->
<link href="http://serverapi.arcgisonline.com/jsapi/arcgis/3.0/js/dojo/dijit/themes/claro/claro.css" rel="stylesheet" type="text/css" />
<link href="http://serverapi.arcgisonline.com/jsapi/arcgis/3.0/js/esri/dijit/css/Popup.css" rel="stylesheet" type="text/css" />
<link href="<?php print $GLOBALS['base_url'] . '/' . drupal_get_path('module', 'esri_mapgallery'); ?>/css/reset.css" rel="stylesheet" type="text/css" />
<link href="<?php print $GLOBALS['base_url'] . '/' . drupal_get_path('module', 'esri_mapgallery'); ?>/css/960.css" rel="stylesheet" type="text/css" />
<link href="<?php print $GLOBALS['base_url'] . '/' . drupal_get_path('module', 'esri_mapgallery'); ?>/css/common.css" rel="stylesheet" type="text/css" />
<link href="<?php print $GLOBALS['base_url'] . '/' . drupal_get_path('module', 'esri_mapgallery'); ?>/css/themes.css" rel="stylesheet" type="text/css" />
<!--[if gte IE 9]>
<link href="<?php print $GLOBALS['base_url'] . '/' . drupal_get_path('module', 'esri_mapgallery'); ?>/css/ie9.css" rel="stylesheet" type="text/css" />
<![endif]-->
<!-- END STYLESHEETS -->
<!-- JAVASCRIPT -->
<script type="text/javascript" src="<?php print $GLOBALS['base_url'] . '/' . drupal_get_path('module', 'esri_mapgallery'); ?>/javascript/djConfig.js"></script>
<script type="text/javascript" src="http://serverapi.arcgisonline.com/jsapi/arcgis/?v=3.0"></script>
<script type="text/javascript" src="<?php print $GLOBALS['base_url'] . '/' . drupal_get_path('module', 'esri_mapgallery'); ?>/javascript/common.js"></script>
<script type="text/javascript" src="<?php print $GLOBALS['base_url'] . '/' . drupal_get_path('module', 'esri_mapgallery'); ?>/javascript/map/layout.js"></script>
<!-- END JAVASCRIPT -->

    <div id="mainPanel" class="grid_7">
      <h1 class="mapHeader" id="title"></h1>
      <div id="subtitle"></div>
      <div class="mapContainer">
        <div id="map"></div>
      </div>
    </div>
    <div id="sidePanelDrupal" class="grid_7 contentleft dataLayers">
      <div id="tabMenu" class="outerHeight"></div>
      <div id="legendMenu" class="tabMenu defaultMenu scrollHeight">
        <div id="legendDiv">
          <div id="mapLayers"></div>
          <h2 id="legendHeader"></h2>
          <div id="legendContent"></div>
        </div>
        <div class="clear"></div>
      </div>
      <div id="aboutMenu" class="tabMenu scrollHeight">
        <div id="description">
          <div id="descriptionContent"></div>
          <div id="mapMoreInfo"></div>
        </div>
<!--
          <div class="clear"></div>
-->
      </div>
    </div>
<!--
  <div id="mapContent">
    <div class="">
  </div>
</div>
-->