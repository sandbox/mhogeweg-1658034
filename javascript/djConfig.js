// host path regular expression
var pathRegex = new RegExp(/\/[^\/]+$/);
var locationPath = location.pathname.replace(pathRegex, '');
//var modulePath = 'http://' + location.host + locationPath.replace('arcgis/gallery','').replace('arcgis/map','') + '/sites/all/modules/esri_mapgallery';
var modulePath = 'http://' + location.host + locationPath.substring(0,locationPath.indexOf('/arcgis')) + '/sites/all/modules/esri_mapgallery';

// Dojo Config
var dojoConfig = {
    parseOnLoad: true,
    packages: [{
        name: "esriTemplate",
        location: modulePath
    }, {
        name: "myModules",
        location: locationPath + '/javascript'
    }, {
        name: "apl",
        location: locationPath + '/apl'
    }]
};

// Global Variables
var i18n, searchVal = '', dataOffset, prevVal = "", ACObj, ACTimeout, timer, urlObject, portal, map, locateResultLayer, aoGeocoder, aoGeoCoderAutocomplete, mapFullscreen, resizeTimer, mapCenter;