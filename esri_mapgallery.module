<?php

/**
 * @file
 * Module file for esri_mapgallery_module.
 */

/**
 * @defgroup esri_mapgallery Example: Page
 * @ingroup ArcGIS
 * @{
 * This example demonstrates how a module can display a map gallery at a given URL.
 *
 */

/**
 * Implements hook_help().
 */
function esri_mapgallery_help($path, $arg) {
  switch ($path) {
    case 'arcgis/about':
      // Help text for the simple page registered for this path.
      return t('This is help text for the ArcGIS Online module for Drupal.');

    case 'arcgis/map':
      // Help text for the simple page registered for this path.
      return t('This is help text for the Webmap Page.');

    case 'arcgis/gallery':
      // Help text for the simple page registered for this path.
      return t('This is help text for the Gallery page.');

    case 'admin/help#esri_mapgallery':
      // Help text for the admin section, using the module name in the path.
      return t('This is help text created in the page example\'s second case.');
  }
}

/**
 * Implements hook_permission().
 */
function esri_mapgallery_permission() {
  return array(
    'access_map_page' => array(
      'title' => t('Access map page'),
      'description' => t('Allow users to access the Webmap Viewer page'),
    ),
    'access_gallery_page' => array(
      'title' => t('Access gallery page'),
      'description' => t('Allow users to access the Map Gallery page'),
    ),
  );
}

/**
 * Implements hook_theme().
 */
function esri_mapgallery_theme() {
  return array(
    'gallery' => array(
      'template' => 'gallery',
      'variables' => array('title' => NULL, 'groupID' => '69b91f7b857b40a484c4aacbd1b243a7')
    ),
    'map' => array(
      'template' => 'map',
      'variables' => array('title' => NULL, 'groupID' => '69b91f7b857b40a484c4aacbd1b243a7', 'mapID' => '085648dddb0e41baa898b5e0b3afc902')
    )
  );
}


/**
 * Implements hook_menu().
 *
 * @see hook_menu()
 * @see menu_example
 */
function esri_mapgallery_menu() {

  // This is the minimum information you can provide for a menu item. This menu
  // item will be created in the default menu, usually Navigation.
  $items['arcgis/about'] = array(
    'title' => 'Map Gallery',
    'page callback' => 'esri_mapgallery_description',
    'access callback' => TRUE,
    'expanded' => TRUE,
  );

  // Webmap Page with simple map template /map/<groupid>/<mapid>
  $items['arcgis/map/%/%'] = array(
    'title' => 'View Map',
    'page callback' => 'esri_mapgallery_map',
    'page arguments' => array(2,3),
    'access arguments' => array('access_map_page'),
    'type' => MENU_CALLBACK,
  );

  // Gallery URL with groupid, page size, and start page
  $items['arcgis/gallery/%'] = array(
    'page callback' => 'esri_mapgallery_gallery',
    'page arguments' => array(2),
    'access arguments' => array('access_gallery_page'),
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Constructs a descriptive page.
 */
function esri_mapgallery_description() {
  return array('#markup' => t('The esri_mapgallery provides two pages, "map" and "gallery". The <a href="@map_link">map page</a> just returns a renderable array for display. The <a href="@gallery_link">arguments page</a> takes two arguments and displays them, as in @gallery_link', array('@map_link' => url('arcgis/map/d4a907a736404636a4f6a9875a19aa8a', array('absolute' => TRUE)), '@gallery_link' => url('arcgis/gallery/5/7', array('absolute' => TRUE)))));
}

/**
 * Constructs a simple webmap page.
 */
function esri_mapgallery_map($groupID, $mapID) {
  // $mapID = id for the map to show
  
  // Ensure the $groupID and $mapID are actual id's.
  if (!is_agolid($groupID) || !is_agolid($mapID)) {
    // show a standard "access denied" page in this case.
    drupal_access_denied();
    return;  // We actually don't get here.
  }
  
  
  // get paths
  
  $modulePath = $GLOBALS['base_url'] . '/' . drupal_get_path('module', 'esri_mapgallery');
  $build['esri_mapgallery_gallery'] = array(
    '#theme' => 'map',
    '#title' => '',
    '#groupID' => $groupID,
    '#mapID' => $mapID
  );
  $build['esri_mapgallery_map']['#attached']['library'][] = array('system', 'ui.mapBody');
  
  // insert a call to a function
  $build['esri_mapgallery_map']['#attached']['js'][] = array('data' => '(function($){$(function() { setConfigOptions(\'' . $groupID . '\', \'' . $mapID . '\', \'' . $modulePath . '\'); })})(jQuery);', 'type' => 'inline');
  $output = drupal_render($build);

  return $output;
}

/**
 * Gallery page callback.
 */
function esri_mapgallery_gallery($groupID) {
  // $groupID = group id for which the gallery will be created
  
  $yesno = is_agolid($groupID);
  
  // if the groupID is not a valid ArcGIS Online id (guid, no - and no {}) then just render the value.
  if (!$yesno) {
    return array('#markup' => t('@groupID is not a valid group id!', array('@groupID' => $groupID)));
  }
  
  // get paths
  
  $modulePath = $GLOBALS['base_url'] . '/' . drupal_get_path('module', 'esri_mapgallery');
  $build['esri_mapgallery_gallery'] = array(
    '#theme' => 'gallery',
    '#title' => '',
    '#groupID' => $groupID
  );
  $build['esri_mapgallery_gallery']['#attached']['library'][] = array('system', 'ui.galleryBody');
  
  // insert a call to a function
  $build['esri_mapgallery_gallery']['#attached']['js'][] = array('data' => '(function($){$(function() { setConfigOptions(\'' . $groupID . '\', \'' . $modulePath . '\'); })})(jQuery);', 'type' => 'inline');
  $output = drupal_render($build);

  return $output;
}

/**
 * Check to ensure the IDs passed in through the URL are ArcGIS Online IDs.
 * These IDs are bare bones guids without { - } characters.
 */
function is_agolid($candidateID) {
  if (preg_match('/^[a-f0-9]{32}$/', strtolower($candidateID))) {
    return TRUE;
  } else {
    return FALSE;
  }
}

/**
 * @} End of "defgroup esri_mapgallery".
 */
