<?php

/**
 * @file
 * Template file for gallery.
 */

?>
<script type="text/javascript">

var configOptions;
var modulePath;

function setConfigOptions(groupID, thePath) {
  modulePath = thePath;
  
  configOptions = {
   "modulePath": thePath,
   "group":groupID,
   "appid":"",
   "theme":"custom",
   "siteTitle":"",
   "siteBannerImage":"",
   "mapTitle":"",
   "mapSnippet":"",
   "mapItemDescription":"",
   "homeHeading":"",
   "homeSnippet":"",
   "homeSideHeading":"Description",
   "homeSideContent":"",
   "footerHeading":"",
   "footerDescription":"",
   "footerLogo":"",
   "footerLogoUrl":"",
   "addThisProfileId":"xa-4f3bf72958320e9e",
   "defaultLayout":"list",
   "sortField":"modified",
   "sortOrder":"desc",
   "searchType":"Web Map",
   "mapViewer":"drupal",
   "galleryItemsPerPage":9,
   "showProfileUrl":true,
   "showAboutPage":false,
   "showSocialButtons":true,
   "showFooter":false,
   "showBasemapGallery":false,
   "showGroupSearch":true,
   "showMapSearch":true,
   "showLayerToggle":true,
   "showLayoutSwitch":true,
   "showOverviewMap":true,
   "showMoreInfo":true,
   "showPagination":true,
   "showExplorerButton":false,
   "showArcGISOnlineButton":false,
   "showMobileButtons":false,
   "openGalleryItemsNewWindow":false,
   "bingMapsKey":"",
   "proxyUrl":"",
   "locatorserviceurl":"http://tasks.arcgis.com/ArcGIS/rest/services/WorldLocator/GeocodeServer/",
   "geometryserviceurl":"http://tasks.arcgisonline.com/ArcGIS/rest/services/Geometry/GeometryServer",
   "sharingurl":"http://www.arcgis.com/sharing/rest/content/items",
   "portalUrl":"http://www.arcgis.com/",
   "mobilePortalUrl":"arcgis://www.arcgis.com/",
   "iosAppUrl":"itms://itunes.apple.com/us/app/arcgis/id379687930?mt=8",
   "androidAppUrl":"https://market.android.com/details?id=com.esri.android.client",
   "pointGraphic":"images/ui/bluepoint-21x25.png"
  }
}
</script>
<!-- STYLESHEETS -->
<link href="http://serverapi.arcgisonline.com/jsapi/arcgis/3.0/js/dojo/dijit/themes/claro/claro.css" rel="stylesheet" type="text/css" />
<link href="<?php print $GLOBALS['base_url'] . '/' . drupal_get_path('module', 'esri_mapgallery'); ?>/css/reset.css" rel="stylesheet" type="text/css" />
<link href="<?php print $GLOBALS['base_url'] . '/' . drupal_get_path('module', 'esri_mapgallery'); ?>/css/960.css" rel="stylesheet" type="text/css" />
<link href="<?php print $GLOBALS['base_url'] . '/' . drupal_get_path('module', 'esri_mapgallery'); ?>/css/common.css" rel="stylesheet" type="text/css" />
<link href="<?php print $GLOBALS['base_url'] . '/' . drupal_get_path('module', 'esri_mapgallery'); ?>/css/themes.css" rel="stylesheet" type="text/css" />
<!--[if gte IE 9]>
<link href="<?php print $GLOBALS['base_url'] . '/' . drupal_get_path('module', 'esri_mapgallery'); ?>/css/ie9.css" rel="stylesheet" type="text/css" />
<![endif]-->

<!-- JAVASCRIPT -->

<script type="text/javascript" src="<?php print $GLOBALS['base_url'] . '/' . drupal_get_path('module', 'esri_mapgallery'); ?>/javascript/djConfig.js"></script>
<script type="text/javascript" src="http://serverapi.arcgisonline.com/jsapi/arcgis/?v=3.0"></script>
<script type="text/javascript" src="<?php print $GLOBALS['base_url'] . '/' . drupal_get_path('module', 'esri_mapgallery'); ?>/javascript/common.js"></script>
<script type="text/javascript" src="<?php print $GLOBALS['base_url'] . '/' . drupal_get_path('module', 'esri_mapgallery'); ?>/javascript/home/layout.js"></script>
<!-- END JAVASCRIPT -->


  <div id="galleryContent">
    <div class="container_12">
      <div id="mainPanel" class="grid_9">
        <p id="homeSnippet"></p>
        <div id="layoutAndSearch"></div>
        <div id="featuredMaps">
          <p id="featuredLoading" class="featuredLoading"></p>
        </div>
        <div class="clear"></div>
        <div class="grid_6 alpha">
          <div id="maps_pagination" class="pagination"></div>
        </div>
      </div>
    </div>
  </div>
